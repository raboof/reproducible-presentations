#+TITLE: Debugging Reproducible Builds One Day at a Time
#+AUTHOR: Vagrant Cascadian
#+EMAIL: vagrant@reproducible-builds.org
#+DATE: SeaGL, 2021-11-06
#+LANGUAGE:  en
#+OPTIONS:   H:1 num:t toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+OPTIONS: ^:nil
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [bigger]
#+latex_header: \mode<beamer>{\usetheme{Madrid}}
#+LaTeX_CLASS_OPTIONS: [aspectratio=169]
#+BEGIN_comment
https://osem.seagl.org/conferences/seagl2021/program/proposals/858
Date: 2021 November 6, 14:00 -0700
Duration: 30 min
Room: Room 1

Debugging Reproducible Builds One Day at a Time

Reproducible builds are a set of software development practices that
create an independently-verifiable path from source to binary code.  A
build is reproducible if given the same source code, build environment
and build instructions, any party can recreate bit-by-bit identical
copies of all specified artifacts.

I'd like to share with you my process for going about fixing
Reproducible Builds issues in Debian, though some of the ideas will be
applicable to debugging issues of any kind in any environment.

I'll explore how I go about identifying issues to work on, learn more
about the specific issues, recreate the problem locally, isolate the
potential causes, dissect the problem into identifiable parts, and
adapt the packaging and/or source code to fix the issues.

This will give you an eye into how I think about, struggle with, and
eventually fix all sorts of things.

Watching this talk should help you go from someone who "knows a bit of
code" to someone ready to submit a fix to your favorite free software
project!

This talk will mostly focus on the *hows* of Reproducible Builds, not
too much on the *whys*, which can be further explored at:

  https://reproducible-builds.org/
#+END_comment

* Defining Reproducible Builds

  Reproducible builds are a set of software development practices that
  create an independently-verifiable path from source to binary code.

  https://reproducible-builds.org/

  A build is reproducible if given the same source code, build
  environment and build instructions, any party can recreate
  bit-by-bit identical copies of all specified artifacts.

  https://reproducible-builds.org/docs/definition/

* Identify Broadly

  https://tests.reproducible-builds.org/debian/bookworm/index_suite_amd64_stats.html

  29652 packages (94.2%) successfully built reproducibly in bookworm/amd64.

  899 packages (2.8%) failed to build reproducibly.

  817 packages (2.5%) failed to build from source.

* Identify Broadly But Harder

  https://tests.reproducible-builds.org/debian/unstable/index_suite_amd64_stats.html

  26583 packages (81.6%) successfully built reproducibly in unstable/amd64.

  4307 packages (13.2%) failed to build reproducibly.

  1441 packages (4.4%) failed to build from source.

* Identify Categorically

  https://tests.reproducible-builds.org/debian/bookworm/amd64/index_pkg_sets.html

* Identify Build Essentially

  https://tests.reproducible-builds.org/debian/bookworm/amd64/pkg_set_build-essential.html

  Build Essential

  4 (6.9%) packages failed to build reproducibly

  2 (3.4%) packages failed to build from source

* Identify Essentially Build Essential

  https://tests.reproducible-builds.org/debian/bookworm/amd64/pkg_set_build-essential-depends.html

  117 (3.2%) packages failed to build reproducibly

  95 (2.6%) packages failed to build from source

* Identify Unknowns

  https://tests.reproducible-builds.org/debian/bookworm/amd64/index_no_notes.html

  61 unreproducible packages in bookworm/amd64

  544 FTBFS packages in bookworm/amd64

* Tools to Create Problems

  reprotest

  disorderfs

* Tools to Create Different Problems

  sbuild --chroot-mode=unshare --add-depends=usrmerge ...

* Copyright and attributions
\addtocounter{framenumber}{-1}
\tiny

  Copyright 2021 Vagrant Cascadian <vagrant@reproducible-builds.org>
  Portions by contributors to the reproducible-builds.org website.

  This work is licensed under the Creative Commons
  Attribution-ShareAlike 4.0 International License.

  To view a copy of this license, visit
  https://creativecommons.org/licenses/by-sa/4.0/
