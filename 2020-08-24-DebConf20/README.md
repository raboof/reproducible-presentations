This presentation was created with LaTeX. To build the slides, execute the pdflatex command inside the 2020-08-24-DebConf20 directory:
$ sudo apt install texlive-latex-base texlive-latex-extra lmodern
$ cd /path/2020-08-24-DebConf20
$ pdflatex 2020-08-24-DebConf20.tex

Now you will have a PDF file to view the presentation slides.
